# Brain.js : réseau de neurones en Javascript

![](https://i.imgur.com/oUXdsKy.png)

*&copy; [Julien Noyer](https://www.linkedin.com/in/julien-n-21219b28/) - All rights reserved for educational purposes only*

<br>

## Présentation

[Brain.js](https://brain.js.org/#/) est une librairie à la fois FrontEnd et Backend permettant de créer des réseaux de neurones, de les entrainer et de les utiliser dans le but de réaliser des prédictions. L'intérêt de cette librairie est qu'elle est particulièrement simple à manipuler et qu'elle permet d'appréhender les différentes notions convernant les réseaux de neurones.

<br>

## Contenu du répertoire

Le projet présenté dans ce répertoire permet d'entrainer un réseau de neurones pour reconnaitre des couleurs. Selon quelle soit claire ou foncée, les textes affichés sur la page sont soit noirs, soit blancs.

Pour simplifier l'utilisation de ce répertoire il vous est fortement conseillé d'installer le module [Live Server](https://www.npmjs.com/package/live-server).